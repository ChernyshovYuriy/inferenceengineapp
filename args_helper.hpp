// Copyright (C) 2018 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

/**
 * @brief a header file with common samples functionality
 * @file args_helper.hpp
 */

#include <string>
#include <vector>
#include <gflags/gflags.h>
#include <iostream>
#include <sys/stat.h>

#include <dirent.h>

using namespace std;

/**
* @brief This function checks input args and existence of specified files in a given folder
* @param arg path to a file to be checked for existence
* @return files updated vector of verified input files
*/
void readInputFilesArguments(vector<string> &files, const string &arg) {
    struct stat sb{};
    if (stat(arg.c_str(), &sb) != 0) {
        cout << "File " << arg << " cannot be opened!" << endl;
        return;
    }
    if (S_ISDIR(sb.st_mode)) {
        DIR *dp;
        dp = opendir(arg.c_str());
        if (dp == nullptr) {
            cout << "Directory " << arg << " cannot be opened!" << endl;
            return;
        }

        struct dirent *ep;
        while (nullptr != (ep = readdir(dp))) {
            string fileName = ep->d_name;
            if (fileName == "." || fileName == "..") {
                continue;
            }
            files.push_back(arg + "/" + ep->d_name);
        }
        closedir(dp);
    } else {
        files.push_back(arg);
    }

    if (files.size() < 20) {
        cout << "Files were added: " << files.size() << endl;
        for (const string &filePath : files) {
            cout << "    " << filePath << endl;
        }
    } else {
        cout << "Files were added: " << files.size() << ". Too many to display each of them." << endl;
    }
}

/**
* @brief This function find -i/--images key in input args
*        It's necessary to process multiple values for single key
* @return files updated vector of verified input files
*/
void parseInputFilesArguments(vector<string> &files) {
    vector<string> args = gflags::GetArgvs();
    bool readArguments = false;
    for (auto &arg : args) {
        if (arg == "-i" || arg == "--images") {
            readArguments = true;
            continue;
        }
        if (!readArguments) {
            continue;
        }
        if (arg.c_str()[0] == '-') {
            break;
        }
        readInputFilesArguments(files, arg);
    }
}
