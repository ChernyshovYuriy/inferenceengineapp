// Copyright (C) 2018 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

#include <gflags/gflags.h>
#include <functional>
#include <iostream>
#include <random>
#include <memory>
#include <chrono>

#include <format_reader_ptr.h>
#include <inference_engine.hpp>
#include <ext_list.hpp>

#include <samples/common.hpp>
#include "object_detection_sample_ssd.h"
#include "args_helper.hpp"

using namespace InferenceEngine;
using namespace std;

ConsoleErrorListener error_listener;

bool ParseAndCheckCommandLine(int argc, char *argv[]) {
    // ---------------------------Parsing and validation of input args--------------------------------------
    gflags::ParseCommandLineNonHelpFlags(&argc, &argv, true);
    if (FLAGS_h) {
        showUsage();
        return false;
    }

    cout << "Parsing input parameters" << endl;

    if (FLAGS_ni < 1) {
        throw logic_error("Parameter -ni should be greater than 0 (default: 1)");
    }

    if (FLAGS_i.empty()) {
        throw logic_error("Parameter -i is not set");
    }

    if (FLAGS_m.empty()) {
        throw logic_error("Parameter -m is not set");
    }

    return true;
}

/**
* \brief The entry point for the Inference Engine object_detection sample application
* \file object_detection_sample_ssd/main.cpp
* \example object_detection_sample_ssd/main.cpp
*/
int main(int argc, char *argv[]) {
    try {
        // This sample covers certain topology and cannot be generalized for any object detection one
        cout << "InferenceEngine: " << GetInferenceEngineVersion() << "\n";

        // --------------------------- 1. Parsing and validation of input args ---------------------------------
        if (!ParseAndCheckCommandLine(argc, argv)) {
            return 0;
        }
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 2. Read input -----------------------------------------------------------
        // This vector stores paths to the processed images
        vector<string> images;
        parseInputFilesArguments(images);
        if (images.empty()) {
            throw logic_error("No suitable images were found");
        }
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 3. Load Plugin for inference engine -------------------------------------
        cout << "Loading plugin" << endl;
        InferencePlugin plugin = PluginDispatcher({FLAGS_pp, "../../../lib/intel64", ""}).getPluginByDevice(FLAGS_d);
        if (FLAGS_p_msg) {
            static_cast<InferenceEngine::InferenceEnginePluginPtr>(plugin)->SetLogCallback(error_listener);
        }

        // If CPU device, load default library with extensions that comes with the product
        if (FLAGS_d.find("CPU") != string::npos) {
            // cpu_extensions library is compiled from "extension" folder containing
            // custom MKLDNNPlugin layer implementations. These layers are not supported
            // by mkldnn, but they can be useful for inferring custom topologies.
            plugin.AddExtension(make_shared<Extensions::Cpu::CpuExtensions>());
        }

        if (!FLAGS_l.empty()) {
            // CPU(MKLDNN) extensions are loaded as a shared library and passed as a pointer to base extension
            IExtensionPtr extension_ptr = make_so_pointer<IExtension>(FLAGS_l);
            plugin.AddExtension(extension_ptr);
            cout << "CPU Extension loaded: " << FLAGS_l << endl;
        }

        if (!FLAGS_c.empty()) {
            // clDNN Extensions are loaded from an .xml description and OpenCL kernel files
            plugin.SetConfig({{PluginConfigParams::KEY_CONFIG_FILE, FLAGS_c}});
            cout << "GPU Extension loaded: " << FLAGS_c << endl;
        }

        // Setting plugin parameter for per layer metrics
        if (FLAGS_pc) {
            plugin.SetConfig({{PluginConfigParams::KEY_PERF_COUNT, PluginConfigParams::YES}});
        }

        // Printing plugin version
        printPluginVersion(plugin, cout);
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 4. Read IR Generated by ModelOptimizer (.xml and .bin files) ------------
        string binFileName = fileNameNoExt(FLAGS_m) + ".bin";
        cout << "Loading network files:\n\t" << FLAGS_m << "\n\t" << binFileName << endl;

        CNNNetReader networkReader;
        // Read network model
        networkReader.ReadNetwork(FLAGS_m);

        // Extract model name and load weights
        networkReader.ReadWeights(binFileName);
        CNNNetwork network = networkReader.getNetwork();
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 5. Prepare input blobs --------------------------------------------------
        cout << "Preparing input blobs" << endl;

        // Taking information about all topology inputs
        InputsDataMap inputsInfo(network.getInputsInfo());

        // SSD network has one input and one output
        if (inputsInfo.size() != 1 && inputsInfo.size() != 2) {
            throw logic_error("Sample supports topologies only with 1 or 2 inputs");
        }

        //Some networks have SSD-like output format (ending with DetectionOutput layer), but
        //having 2 inputs as Faster-RCNN: one for image and one for "image info".
        //
        //Although object_datection_sample_ssd's main task is to support clean SSD, it could score
        //the networks with two inputs as well. For such networks imInfoInputName will contain the "second" input name.
        string imageInputName, imInfoInputName;

        InputInfo::Ptr inputInfo = inputsInfo.begin()->second;

        SizeVector inputImageDims;
        // Stores input image

        // Iterating over all input blobs
        for (auto &item : inputsInfo) {
            // Working with first input tensor that stores image
            if (item.second->getInputData()->getTensorDesc().getDims().size() == 4) {
                imageInputName = item.first;

                cout << "Batch size is " << to_string(networkReader.getNetwork().getBatchSize()) << endl;

                // Creating first input blob
                Precision inputPrecision = Precision::U8;
                item.second->setPrecision(inputPrecision);
            } else if (item.second->getInputData()->getTensorDesc().getDims().size() == 2) {
                imInfoInputName = item.first;

                Precision inputPrecision = Precision::FP32;
                item.second->setPrecision(inputPrecision);
                if ((item.second->getTensorDesc().getDims()[1] != 3 &&
                     item.second->getTensorDesc().getDims()[1] != 6) ||
                    item.second->getTensorDesc().getDims()[0] != 1) {
                    throw logic_error("Invalid input info. Should be 3 or 6 values length");
                }
            }
        }
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 6. Prepare output blobs -------------------------------------------------
        cout << "Preparing output blobs" << endl;

        OutputsDataMap outputsInfo(network.getOutputsInfo());

        string outputName;
        DataPtr outputInfo;
        for (const auto &out : outputsInfo) {
            if (out.second->creatorLayer.lock()->type == "DetectionOutput") {
                outputName = out.first;
                outputInfo = out.second;
            }
        }

        if (outputInfo == nullptr) {
            throw logic_error("Can't find a DetectionOutput layer in the topology");
        }

        const SizeVector outputDims = outputInfo->getTensorDesc().getDims();

        const int maxProposalCount = outputDims[2];
        const int objectSize = outputDims[3];

        if (objectSize != 7) {
            throw logic_error("Output item should have 7 as a last dimension");
        }

        if (outputDims.size() != 4) {
            throw logic_error("Incorrect output dimensions for SSD model");
        }

        // Set the precision of output data provided by the user, should be called before load of the network to the
        // plugin
        outputInfo->setPrecision(Precision::FP32);
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 7. Loading model to the plugin ------------------------------------------
        cout << "Loading model to the plugin" << endl;

        ExecutableNetwork executable_network = plugin.LoadNetwork(network, {});
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 8. Create infer request -------------------------------------------------
        InferRequest infer_request = executable_network.CreateInferRequest();
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 9. Prepare input --------------------------------------------------------
        // Collect images data ptrs
        vector<shared_ptr<unsigned char>> imagesData, originalImagesData;
        vector<int> imageWidths, imageHeights;
        for (auto &image : images) {
            FormatReader::ReaderPtr reader(image.c_str());
            if (reader.get() == nullptr) {
                cout << "Image " + image + " cannot be read!" << endl;
                continue;
            }
            // Store image data
            shared_ptr<unsigned char> originalData(reader->getData());
            shared_ptr<unsigned char> data(
                    reader->getData(inputInfo->getTensorDesc().getDims()[3], inputInfo->getTensorDesc().getDims()[2])
            );
            if (data != nullptr) {
                originalImagesData.push_back(originalData);
                imagesData.push_back(data);
                imageWidths.push_back(reader->width());
                imageHeights.push_back(reader->height());
            }
        }
        if (imagesData.empty()) {
            throw logic_error("Valid input images were not found!");
        }

        size_t batchSize = network.getBatchSize();
        cout << "Batch size is " << to_string(batchSize) << endl;
        if (batchSize != imagesData.size()) {
            cout << "Number of images " + to_string(imagesData.size()) + \
                " doesn't match batch size " + to_string(batchSize) << endl;
            batchSize = min(batchSize, imagesData.size());
            cout << "Number of images to be processed is " << to_string(batchSize) << endl;
        }

        // Creating input blob
        Blob::Ptr imageInput = infer_request.GetBlob(imageInputName);

        // Filling input tensor with images. First b channel, then g and r channels
        size_t num_channels = imageInput->getTensorDesc().getDims()[1];
        size_t image_size = imageInput->getTensorDesc().getDims()[3] * imageInput->getTensorDesc().getDims()[2];

        unsigned char *data = static_cast<unsigned char *>(imageInput->buffer());

        // Iterate over all input images
        for (size_t image_id = 0; image_id < min(imagesData.size(), batchSize); ++image_id) {
            // Iterate over all pixel in image (b,g,r)
            for (size_t pid = 0; pid < image_size; pid++) {
                // Iterate over all channels
                for (size_t ch = 0; ch < num_channels; ++ch) {
                    // [images stride + channels stride + pixel id ] all in bytes
                    data[image_id * image_size * num_channels + ch * image_size + pid] = imagesData.at(image_id).get()[
                            pid * num_channels + ch];
                }
            }
        }

        if (!imInfoInputName.empty()) {
            Blob::Ptr input2 = infer_request.GetBlob(imInfoInputName);
            auto imInfoDim = inputsInfo.find(imInfoInputName)->second->getTensorDesc().getDims()[1];

            // Fill input tensor with values
            auto *p = input2->buffer().as<PrecisionTrait<Precision::FP32>::value_type *>();

            for (size_t image_id = 0; image_id < min(imagesData.size(), batchSize); ++image_id) {
                p[image_id * imInfoDim +
                  0] = static_cast<float>(inputsInfo[imageInputName]->getTensorDesc().getDims()[2]);
                p[image_id * imInfoDim +
                  1] = static_cast<float>(inputsInfo[imageInputName]->getTensorDesc().getDims()[3]);
                for (int k = 2; k < imInfoDim; k++) {
                    p[image_id * imInfoDim + k] = 1.0f;  // all scale factors are set to 1.0
                }
            }
        }
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 10. Do inference ---------------------------------------------------------
        cout << "Start inference (" << FLAGS_ni << " iterations)" << endl;

        typedef chrono::high_resolution_clock Time;
        typedef chrono::duration<double, ratio<1, 1000>> ms;
        typedef chrono::duration<float> fsec;

        double total = 0.0;
        // Start inference & calc performance
        for (int iter = 0; iter < FLAGS_ni; ++iter) {
            auto t0 = Time::now();
            infer_request.Infer();
            auto t1 = Time::now();
            fsec fs = t1 - t0;
            ms d = chrono::duration_cast<ms>(fs);
            total += d.count();
        }
        // -----------------------------------------------------------------------------------------------------

        // --------------------------- 11. Process output -------------------------------------------------------
        cout << "Processing output blobs" << endl;

        const Blob::Ptr output_blob = infer_request.GetBlob(outputName);
        const float *detection = static_cast<PrecisionTrait<Precision::FP32>::value_type *>(output_blob->buffer());

        vector<vector<int> > boxes(batchSize);
        vector<vector<int> > classes(batchSize);

        // Each detection has image_id that denotes processed image
        for (int curProposal = 0; curProposal < maxProposalCount; curProposal++) {
            float image_id = detection[curProposal * objectSize + 0];
            if (image_id < 0) {
                break;
            }

            float label = detection[curProposal * objectSize + 1];
            float confidence = detection[curProposal * objectSize + 2];
            float xmin = detection[curProposal * objectSize + 3] * imageWidths[image_id];
            float ymin = detection[curProposal * objectSize + 4] * imageHeights[image_id];
            float xmax = detection[curProposal * objectSize + 5] * imageWidths[image_id];
            float ymax = detection[curProposal * objectSize + 6] * imageHeights[image_id];

            cout << "[" << curProposal << "," << label << "] element, prob = " << confidence <<
                 "    (" << xmin << "," << ymin << ")-(" << xmax << "," << ymax << ")" << " batch id : " << image_id;

            if (confidence > 0.5) {
                // Drawing only objects with >50% probability
                classes[image_id].push_back(static_cast<int>(label));
                boxes[image_id].push_back(static_cast<int>(xmin));
                boxes[image_id].push_back(static_cast<int>(ymin));
                boxes[image_id].push_back(static_cast<int>(xmax - xmin));
                boxes[image_id].push_back(static_cast<int>(ymax - ymin));
                cout << " WILL BE PRINTED!";
            }
            cout << endl;
        }

        for (size_t batch_id = 0; batch_id < batchSize; ++batch_id) {
            addRectangles(originalImagesData[batch_id].get(), imageHeights[batch_id], imageWidths[batch_id],
                          boxes[batch_id], classes[batch_id],
                          BBOX_THICKNESS);
            const string image_path = "out_" + to_string(batch_id) + ".bmp";
            if (writeOutputBmp(image_path, originalImagesData[batch_id].get(), imageHeights[batch_id],
                               imageWidths[batch_id])) {
                cout << "Image " + image_path + " created!" << endl;
            } else {
                throw logic_error(string("Can't create a file: ") + image_path);
            }
        }
        // -----------------------------------------------------------------------------------------------------
        cout << endl << "total inference time: " << total << endl;
        cout << "Average running time of one iteration: " << total / static_cast<double>(FLAGS_ni) << " ms" << endl;
        cout << endl << "Throughput: " << 1000 * static_cast<double>(FLAGS_ni) * batchSize / total << " FPS" << endl;
        cout << endl;

        // Show performance results
        if (FLAGS_pc) {
            printPerformanceCounts(infer_request, cout);
        }
    }
    catch (const exception &error) {
        cerr << error.what() << endl;
        return 1;
    }
    catch (...) {
        cerr << "Unknown/internal exception happened." << endl;
        return 1;
    }

    cout << "Execution successful" << endl;
    return 0;
}
